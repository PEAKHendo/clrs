﻿using System;
using System.Data;
using Microsoft.SqlServer.Server;
using System.Data.SqlTypes;
using System.Text;


public partial class JaroWinkler
{

    private static StringBuilder commonCharacters(string Word1, string Word2, int Window)
    {
        int charactersFound = 0;
        StringBuilder returnStringBuilder = new StringBuilder();                                        // instantiate the return value
        if (!(string.IsNullOrEmpty(Word1) || string.IsNullOrEmpty(Word2)))                              // first check there is anything to test
        {                                                                                               // Both words exist so can start comparing                                                      
            StringBuilder copyWord1 = new StringBuilder(Word1);                                         // we don't need to convert Word1 to a stringbuilder object but it helps
            StringBuilder copyWord2 = new StringBuilder(Word2);                                         // we need to convert Word2 to a stringbuilder object so we can work with the string
            for (int i = 0; i < copyWord1.Length; i++)                                                  // for the length of Word1
            {
                bool foundIT = false;                                                                   // reset the foundIT flag
                int j = (i - Window > 0) ? i - Window : 0;                                              // set up j - if its more than halfway then start from halfway
                while (
                    j <= (                                                                             // j less than
                            (i + Window <= copyWord2.Length)                                            // if i + window less than the length of copyWord2
                            ? i + Window - 1                                                               // just use i + window 
                            : copyWord2.Length - 1                                                         // otherwise just go the end of copyWord2
                           )
                    && foundIT == false)                                                                // if still some copyWord2 to check and not found anything keep going
                {
                    if (copyWord2[j] == copyWord1[i])                                                    // if its found
                    {
                        returnStringBuilder.Append(copyWord2[j]);                                       // append the found character
                        charactersFound += 1;                                                           // increment the characters found                            
                        foundIT = true;                                                                 // flag that
                        copyWord2[j] = '\t';                                                            // replace the charater with some junk so we don't find it again                            
                    }
                    j += 1;                                                                             // increment j
                }
            }

        }
        return returnStringBuilder;
    }

    private static int calculateTranspositions(StringBuilder Common1, StringBuilder Common2)
    {
        int returnValue = 0;                                                                                    // instantiate return value
        for (int i = 0; i < Common1.Length; i++)
        {
            if (Common1[i] != Common2[i])
            {
                returnValue += 1;
            }
        }
        return returnValue / 2;
    }

    private static float calculateJaroDistance(string Word1, string Word2)
    {
        float returnValue = 0; 
        int Window = (Word1.Length >= Word2.Length) ? (Word1.Length / 2) - 1 : (Word2.Length / 2) - 1;
        StringBuilder Common1 = commonCharacters(Word1, Word2, Window);
        if (Common1.Length != 0)
        {       
            StringBuilder Common2 = commonCharacters(Word2, Word1, Window);  
            if (Common2.Length != 0 && Common2.Length == Common1.Length)  // no idea how either of these could be true but as well to add to check just in case
            {
                returnValue =   Common1.Length / (3F * Word1.Length) + 
                                Common1.Length / (3F * Word2.Length) + 
                                (Common1.Length - calculateTranspositions(Common1, Common2))/ (3F * Common1.Length);

            }
        }

        return returnValue;

    }

    private static int calculatePrefixLength(string Word1, string Word2)
    {
        int minPrefixLength = 4; 
        StringBuilder copyWord1 = new StringBuilder(Word1);    
        StringBuilder copyWord2 = new StringBuilder(Word2);                                               
        if (copyWord1.Length * copyWord2.Length != 0)
        {
            int n = 0;
            if (minPrefixLength < copyWord1.Length && minPrefixLength < copyWord2.Length) 
                    n = minPrefixLength;
            else    n = (copyWord1.Length < copyWord2.Length) ? copyWord1.Length : copyWord2.Length;                    
            int i = 0;
            while (i < n)
            {
                if (copyWord1[i] != copyWord2[i])
                {
                    minPrefixLength = i;
                    i = n;
                }
                i += 1; 
            }
        }
        return minPrefixLength;
    }
    [Microsoft.SqlServer.Server.SqlFunction(DataAccess = DataAccessKind.None)]
    public static Double calculateJaroWinkler(string Word1, string Word2)
    {
        float returnValue = 0F;
        float JaroDistance = calculateJaroDistance(Word1, Word2);

        returnValue = JaroDistance + (calculatePrefixLength(Word1, Word2) * 0.1F) * (1.0F - JaroDistance);
        return returnValue;

    }
}

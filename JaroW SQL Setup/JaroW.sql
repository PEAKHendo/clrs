CREATE ASSEMBLY JaroWinkler from 'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Binn\CLRCustomisations\JaroWinkler.dll' WITH PERMISSION_SET = SAFE  
GO
CREATE Function JaroWinkler (@Word1 nvarchar(max), @Word2 nvarchar(MAX)) Returns float WITH EXECUTE AS CALLER
AS  
EXTERNAL NAME JaroWinkler.JaroWinkler.calculateJaroWinkler


GO
USE [BODS_Target_Dev]
GO

/****** Object:  UserDefinedFunction [dbo].[RegExpLike]    Script Date: 16/11/2018 12:51:24 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[RegExpLike](@Text [nvarchar](max), @Pattern [nvarchar](511))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlRegularExpressions].[ExtendedTextFunctions.SqlRegularExpressions].[Like]
GO


USE [BODS_Target_Dev]
GO

/****** Object:  UserDefinedFunction [dbo].[RegExpMatches]    Script Date: 16/11/2018 12:50:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[RegExpMatches](@text [nvarchar](max), @pattern [nvarchar](511))
RETURNS  TABLE (
	[Index] [int] NULL,
	[Length] [int] NULL,
	[Value] [nvarchar](255) NULL
) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlRegularExpressions].[ExtendedTextFunctions.SqlRegularExpressions].[GetMatches]
GO

